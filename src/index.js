const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");

const app = express();

const { checkLogin } = require("./config/request");
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan("combined"));
app.use(checkLogin);

const pasien = require("./routes/pasien");
const kamar = require("./routes/kamar");
const darah = require("./routes/darah");
const bpjs = require("./routes/bpjs");
const dokter = require("./routes/dokter");
const poli = require("./routes/poli");
const kiosk = require("./routes/kiosk");
const keuangan = require("./routes/keuangan");

app.use("/api/v1/pasien", pasien);
app.use("/api/v1/pasien", pasien);
app.use("/api/v1/kamar", kamar);
app.use("/api/v1/darah", darah);
app.use("/api/v1/bpjs", bpjs);
app.use("/api/v1/dokter", dokter);
app.use("/api/v1/poli", poli);
app.use("/api/v1/kiosk", kiosk);
app.use("/api/v1/keuangan", keuangan);
app.get("/api/v1/check", (req, res) => {
  res.json({
    cookie: req.headers.cookie,
  });
});

app.get("*", (req, res) => {
  return res.sendFile(__dirname + "/error.html");
});

app.listen(8300, () => console.log(`SERVER RUNNING AT http://localhost:8300`));

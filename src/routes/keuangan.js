const express = require("express");
const axios = require("axios");
const _ = require("lodash");
const route = express.Router();

const { db } = require("../config");

route.get("/", async (req, res, next) => {
  try {
    const data = await db("sim_keuangan_2020.jurnal_umum")
      .select(
        "sim_keuangan_2020.jurnal_umum.no_jurnal",
        "sim_keuangan_2020.jurnal_umum.tgl_jurnal",
        "uraian"
      )
      .groupBy("sim_keuangan_2020.jurnal_umum.no_jurnal")
      .orderBy("id", "no_jurnal");

    const listData = await db("sim_keuangan_2020.jurnal_umum").select(
      "sim_keuangan_2020.jurnal_umum.no_jurnal",
      "sim_keuangan_2020.jurnal_umum.kd_rek_nama",
      "sim_keuangan_2020.jurnal_umum.kd_rek",
      "sim_keuangan_2020.jurnal_umum.debet",
      "sim_keuangan_2020.jurnal_umum.kredit"
    );

    // const checkData = listData.map((uraian) => {
    //   if (_.findIndex(data, { no_jurnal: uraian.no_jurnal }) == -1) {
    //     data.data_rekening.push(uraian)
    //   }
    // })

    const dataKeuangan = data.map((d) => {
      const filter = listData.filter((data) => {
        return data.no_jurnal == d.no_jurnal;
      });
      d.data_keuangan = filter;

      return d;
    });

    return res.json(dataKeuangan);
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

const express = require("express");
const axios = require("axios");
const route = express.Router();

// const { config } = require("../config/request");
const { bpjs } = require("../config");

route.get("/:nokartu", async (req, res, next) => {
  const { nokartu } = req.params;
  try {
    const result = await axios.get(
      `http://192.168.5.49:8301/api/v1/nokartu/${nokartu}`
    );
    res.json(result.data);
  } catch (error) {
    res.json({
      success: false,
      message: error,
    });
  }
  // axios(
  //   config(req, "GET", `http://192.168.5.49:8301/api/v1/nokartu/${nokartu}`)
  // )
  //   .then(function (response) {
  //     res.json(response.data);
  //   })
  //   .catch(function (error) {
  //     res.json({
  //       success: false,
  //       message: error,
  //     });
  //   });
});

route.get("nik/:nik", async (req, res, next) => {
  const { nik } = req.params;
  try {
    const result = await axios.get(
      `http://192.168.5.49:8301/api/v1/nik/${nik}`
    );
    res.json(result.data);
  } catch (error) {
    res.json({
      success: false,
      message: error,
    });
  }
});

route.get("/sep/:nomor", async (req, res, next) => {
  const { nomor } = req.params;
  try {
    const checkSep = await bpjs("pendaftaran.penjamin")
      .join(
        "pendaftaran.pendaftaran",
        "pendaftaran.pendaftaran.NOMOR",
        "pendaftaran.penjamin.NOPEN"
      )

      // .join(
      //   "pendaftaran.pendaftaran",
      //   "pendaftaran.pendaftaran.DIAGNOSA_MASUK",
      //   "master.diagnosa_masuk.ID"
      // )

      .join("bpjs.peserta", "bpjs.peserta.norm", "pendaftaran.pendaftaran.NORM")
      .where("pendaftaran.penjamin.NOMOR", nomor);

    return res.json(checkSep[0]);
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

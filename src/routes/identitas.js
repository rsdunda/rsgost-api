const express = require("express");
const axios = require("axios");
const route = express.Router();

const { master } = require("../config");

route.get("/nik/:nik", async (req, res, next) => {
  const { nik } = req.params;
  try {
    const pasien = master("kartu_identitas_pasien")
      .select("pasien.NORM")
      .join("pasien", "pasien.NORM", "kartu_identitas_pasien.NORM");

    console.log(pasien);
  } catch (error) {}
});

module.exports = route;

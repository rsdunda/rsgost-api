const express = require("express");
const axios = require("axios");
const route = express.Router();

const { config } = require("../config/request")

route.get("/", (req, res, next) => {
  axios(config(req, "GET", `http://192.168.5.49/webservice/informasi/ruangkamartidur`))
    .then(function (response) {
      res.json(response.data);
    })
    .catch(function (error) {
      res.json({
        success: false,
        message: error,
      });
    });
});

module.exports = route;

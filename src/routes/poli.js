const express = require("express");
const route = express.Router();
const _ = require("lodash");
const { master } = require("../config");

route.get("/", async (req, res, next) => {
  try {
    // SELECT * FROM smf_ruangan JOIN referensi ON referensi.ID = smf_ruangan.SMF WHERE referensi.JENIS = 26 AND smf_ruangan.SMF != 31
    // GROUP BY referensi.ID
    const smf = await master("smf_ruangan")
    .select("smf_ruangan.SMF as ID", "referensi.DESKRIPSI as NAMA")
      .join("referensi", "referensi.ID", "smf_ruangan.SMF")
      .where("smf_ruangan.STATUS", 1)
      .where("referensi.JENIS", 26)
      .whereNot("smf_ruangan.SMF", 31)
      .whereNot("smf_ruangan.SMF", 28)
      .whereNot("smf_ruangan.SMF", 0)
      .groupBy("referensi.ID")

    //   .select(
    //     "pegawai.ID",
    //     "pegawai.GELAR_DEPAN",
    //     "pegawai.NAMA",
    //     "pegawai.GELAR_BELAKANG",
    //     "pegawai.SMF"
    //   );

    return res.json(smf);
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

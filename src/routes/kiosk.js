const express = require("express");
const route = express.Router();
const moment = require("moment");
const { kiosk } = require("../config");

route.post("/", async (req, res, next) => {
  const { nilai } = req.body;
  // 1 = sangat puas
  // 2 = puas
  // 3 = tidak puas
  // 4 = sangat tidak puas
  try {
    const addNilai = await kiosk("kiosk").insert({
      TANGGAL: moment(new Date()).format(),
      NILAI: nilai,
    });
    if (addNilai) {
      return res.status(201).json({
        status: 201,
        messages: "Success",
      });
    }

    return res.status(500).json({
      status: 500,
      messages: "Error",
    });
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

const express = require("express");
const axios = require("axios");
const route = express.Router();
const { db, master } = require("../config");

const { config } = require("../config/request");

route.get("/:id", (req, res, next) => {
  const { id } = req.params;
  axios(config(req, "GET", `http://192.168.5.49/webservice/pasien/${id}`))
    .then(function (response) {
      res.json(response.data);
    })
    .catch(function (error) {
      res.json({
        success: false,
        message: error,
      });
    });
});

route.get("/nik/:nik", async (req, res, next) => {
  const { nik } = req.params;
  try {
    const pasien = await master("kartu_identitas_pasien")
      .select(
        "pasien.NORM",
        "kartu_identitas_pasien.NOMOR as NIK",
        "pasien.NAMA",
        master.raw(
          'IF(pasien.JENIS_KELAMIN=1, "Laki - Laki", "Perempuan") as KELAMIN'
        ),
        "pasien.ALAMAT"
      )
      .join("pasien", "pasien.NORM", "kartu_identitas_pasien.NORM")
      .where("kartu_identitas_pasien.NOMOR", nik);

    if (pasien.length > 0) {
      return res.json(pasien[0]);
    }

    return res.status(500).json({
      success: false,
      message: "Error",
    });
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

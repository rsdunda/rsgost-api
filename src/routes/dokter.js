const express = require("express");
const route = express.Router();
const _ = require("lodash");
const { master } = require("../config");

route.get("/", async (req, res, next) => {
  try {
    const dokter = await master("pegawai")
      .join("dokter", "dokter.NIP", "pegawai.NIP")
      .where("pegawai.PROFESI", 4)
      .whereNot("pegawai.SMF", 31)
      .whereNot("pegawai.ID", 1)
      .select(
        "pegawai.ID",
        "pegawai.GELAR_DEPAN",
        "pegawai.NAMA",
        "pegawai.GELAR_BELAKANG",
        "pegawai.SMF"
      );

    return res.json(dokter);
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

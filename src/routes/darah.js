const express = require("express");
const { countBy, drop } = require("lodash");
const route = express.Router();
const _ = require("lodash");
const { utd } = require("../config");

route.get("/", async (req, res, next) => {
  try {
    // GET STOK DARAH
    // A+
    const AP = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 2 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    // B+
    const BP = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 3 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    //AB+
    const ABP = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 4 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    //O+
    const OP = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 6 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    //A-
    const AM = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 7 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    //B-
    const BM = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 8 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    //AB-
    const ABM = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 9 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    //O-
    const OM = await utd("tbl_stok_darah")
      .select("tbl_stok_darah.id_jenisdarah", "m_komponen.nama_komponen")
      .count("* as stok")
      .join(
        "m_komponen",
        "m_komponen.id_komponen",
        "tbl_stok_darah.id_jenisdarah"
      )
      .where({ id_darah: 10 })
      .where({ status: 1 })
      .where({ status_skrining: "Non Reaktif"})
      .orderBy("tbl_stok_darah.id_jenisdarah")
      .groupBy("tbl_stok_darah.id_jenisdarah");

    // // GET PENERIMAAN
    // // A+
    // const PAP = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 2 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // const PBP = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 3 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // //AB+
    // const PABP = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 4 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // //O+
    // const POP = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 6 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // //A-
    // const PAM = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 7 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // //B-
    // const PBM = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 8 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // //AB-
    // const PABM = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 9 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    // //O-
    // const POM = await utd("tbl_penerimaan")
    //   .select(
    //     "tbl_penerimaan.id_komponen as id_jenisdarah",
    //     "m_komponen.nama_komponen"
    //   )
    //   .count("* as stok")
    //   .join(
    //     "m_komponen",
    //     "m_komponen.id_komponen",
    //     "tbl_penerimaan.id_komponen"
    //   )
    //   .where({ id_darah: 10 })
    //   .where({ status: 0 })
    //   .orderBy("tbl_penerimaan.id_komponen")
    //   .groupBy("tbl_penerimaan.id_komponen");

    const darah = await utd("tbl_darah").orderBy("id_darah");

    const komponen = [
      {
        id_jenisdarah: 1,
        nama_komponen: "Whole Blood (WB)",
        stok: 0,
      },
      {
        id_jenisdarah: 2,
        nama_komponen: "Packed Red Cell (PRC)",
        stok: 0,
      },
      {
        id_jenisdarah: 3,
        nama_komponen: "Plasma",
        stok: 0,
      },
      {
        id_jenisdarah: 4,
        nama_komponen: "Trombocyte Concentreate (TC)",
        stok: 0,
      },
    ];

    const checkData = (data) => {
      komponen.map((k) => {
        if (_.findIndex(data, { id_jenisdarah: k.id_jenisdarah }) == -1) {
          data.push(k);
        }
      });
    };

    

    const dataDarah = darah.map((d) => {
      switch (d.id_darah) {
        case 2:
          checkData(AP);
          d.komponen = AP;
          break;
        case 3:
          checkData(BP);
          d.komponen = BP;
          break;
        case 4:
          checkData(ABP);
          d.komponen = ABP;
          break;
        case 6:
          checkData(OP);
          d.komponen = OP;
          break;
        case 7:
          checkData(AM);
          d.komponen = AM;
          break;
        case 8:
          checkData(BM);
          d.komponen = BM;
          break;
        case 9:
          checkData(ABM);
          d.komponen = ABM;
          break;
        case 10:
          checkData(OM);
          d.komponen = OM;
          break;

        default:
          break;
      }

      return d;
    });

    // return res.json(
    //   darah.map((dr) => {
    //     dr.komponen = stok
    //     return dr;
    //   })
    // );

    // const stokDarah = await utd("tbl_stok_darah").select("id_darah", "id_jenisdarah").orderBy("id_darah")
    // const data = stokDarah.filter(darah => darah.id_jenisdarah == 4).map(data => data)
    // console.log(AP)
    return res.json(dataDarah);
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;

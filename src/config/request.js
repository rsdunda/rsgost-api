const axios = require("axios");

module.exports = {
  checkLogin: async (req, res, next) => {
    try {
      const login = await axios.post(
        "http://192.168.5.49/webservice/authentication/login",
        {
          LOGIN: "iwan",
          PASSWORD: "100810",
        }
      );
      res.append("Set-Cookie", `${login.headers["set-cookie"][0]}`);
      return next();
    } catch (error) {
      next(error);
    }
  },

  config: (req, method, url) => {
    return {
      method: method,
      url: url,
      headers: {
        Cookie: req.headers.cookie,
      },
    };
  },
};
